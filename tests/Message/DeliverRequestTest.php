<?php

/**
 * @package Payamak\Ghasedak\Tests\Message
 */

namespace Payamak\Ghasedak\Tests\Message;

use Payamak\Ghasedak\Message\DeliverRequest;
use Payamak\Tests\RequestTestCase;

/**
 * Class DeliverRequestTest
 */
class DeliverRequestTest extends RequestTestCase
{
    /**
     * @var DeliverRequestTest
     */
    protected $request;

    /**
     *
     */
    public function setUp(): void
    {
        $this->request = new DeliverRequest(
            $this->getMethodsClient($this->getMockClient())
        );

        $this->request->initialize(
            [
                'apiKey' => 'apikey',
                'id' => '2025',
                'type' => '1',
            ]
        );
    }

    /**
     * @test
     */
    public function testGetData()
    {
        $data = $this->request->getData();

        $this->assertSame('apikey', $data['apiKey']);
        $this->assertSame('2025', $data['id']);
        $this->assertSame('1', $data['type']);
    }

    /**
     * @test
     */
    public function testSend()
    {
        $this->setMockHttpResponse('DeliverSuccess.txt');
        $response = $this->request->send();

        $this->assertIsBool($response->isSuccessful());
    }
}
