<?php

/**
 * @package Payamak\Ghasedak\Tests\Message
 */

namespace Payamak\Ghasedak\Tests\Message;

use Exception;
use Http\Client\Common\HttpMethodsClient;
use Payamak\Common\Exception\InvalidResponseException;
use Payamak\Ghasedak\Message\AbstractRequest;
use Payamak\Tests\TestCase;

/**
 * Class AbstractRequestTest
 */
class AbstractRequestTest extends TestCase
{
    /**
     * @test
     */
    public function testSendDataException()
    {
        $httpClient = $this->createMock(HttpMethodsClient::class);
        $httpRequest = $this->getMockForAbstractClass(AbstractRequest::class, [$httpClient]);

        $httpClient->method('post')->willThrowException(new Exception('Something went wrong'));

        $this->expectException(InvalidResponseException::class);
        $this->expectExceptionMessage('Something went wrong');

        $httpRequest->send();
    }
}
