<?php

/**
 * @package Payamak\Ghasedak\Tests\Message
 */

namespace Payamak\Ghasedak\Tests\Message;

use Payamak\Ghasedak\Message\DeliverRequest;
use Payamak\Ghasedak\Message\DeliverResponse;
use Payamak\Tests\TestCase;

/**
 * Class DeliverResponseTest
 */
class DeliverResponseTest extends TestCase
{
    /**
     * @test
     */
    public function testDeliverSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('DeliverSuccess.txt');
        $mockRequest = $this->createMock(DeliverRequest::class);
        $jsonContent = json_decode($httpResponse->getBody()->getContents(), true);

        $response = new DeliverResponse($mockRequest, $jsonContent);

        $this->assertIsBool($response->isSuccessful());
        $this->assertEquals("success", $response->getMessage());
    }

    /**
     * @test
     */
    public function testDeliverFailure()
    {
        $httpResponse = $this->getMockHttpResponse('DeliverFailure.txt');
        $mockRequest = $this->createMock(DeliverRequest::class);
        $jsonContent = json_decode($httpResponse->getBody()->getContents(), true);

        $response = new DeliverResponse($mockRequest, $jsonContent);

        $this->assertIsBool($response->isSuccessful());
        $this->assertSame('Invalid Parameters', $response->getMessage());
    }
}
