<?php

/**
 * @package Payamak\Ghasedak\Tests\Message
 */

namespace Payamak\Ghasedak\Tests\Message;

use Payamak\Ghasedak\Message\MessageRequest;
use Payamak\Ghasedak\Message\MessageResponse;
use Payamak\Tests\TestCase;

/**
 * Class MessageResponseTest
 */
class MessageResponseTest extends TestCase
{
    /**
     * @test
     */
    public function testMessageSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('MessageSuccess.txt');
        $mockRequest = $this->createMock(MessageRequest::class);
        $jsonContent = json_decode($httpResponse->getBody()->getContents(), true);

        $response = new MessageResponse($mockRequest, $jsonContent);

        $this->assertIsBool($response->isSuccessful());
        $this->assertEquals("success", $response->getMessage());
    }

    /**
     * @test
     */
    public function testMessageFailure()
    {
        $httpResponse = $this->getMockHttpResponse('MessageFailure.txt');
        $mockRequest = $this->createMock(MessageRequest::class);
        $jsonContent = json_decode($httpResponse->getBody()->getContents(), true);

        $response = new MessageResponse($mockRequest, $jsonContent);

        $this->assertIsBool($response->isSuccessful());
        $this->assertSame('Unauthorized.Invalid Api key', $response->getMessage());
    }
}
