<?php

/**
 * @package Payamak\Ghasedak\Tests\Message
 */

namespace Payamak\Ghasedak\Tests\Message;

use Payamak\Ghasedak\Message\BalanceRequest;
use Payamak\Tests\RequestTestCase;

/**
 * Class BalanceRequestTest
 */
class BalanceRequestTest extends RequestTestCase
{
    /**
     * @var BalanceRequestTest
     */
    protected $request;

    /**
     *
     */
    public function setUp(): void
    {
        $this->request = new BalanceRequest(
            $this->getMethodsClient($this->getMockClient())
        );

        $this->request->initialize(
            [
                'apiKey' => 'apikey',
            ]
        );
    }

    /**
     * @test
     */
    public function testGetData()
    {
        $data = $this->request->getData();

        $this->assertSame('apikey', $data['apiKey']);
    }

    /**
     * @test
     */
    public function testSend()
    {
        $this->setMockHttpResponse('BalanceSuccess.txt');
        $response = $this->request->send();

        $this->assertIsBool($response->isSuccessful());
    }
}
