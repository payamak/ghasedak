<?php

/**
 * @package Payamak\Ghasedak\Tests\Message
 */

namespace Payamak\Ghasedak\Tests\Message;

use Payamak\Ghasedak\Message\MessageRequest;
use Payamak\Tests\RequestTestCase;

/**
 * Class MessageRequestTest
 */
class MessageRequestTest extends RequestTestCase
{
    /**
     * @var MessageRequestTest
     */
    protected $request;

    /**
     *
     */
    public function setUp(): void
    {
        $this->request = new MessageRequest(
            $this->getMethodsClient($this->getMockClient())
        );

        $this->request->initialize(
            array(
                'apiKey' => 'apikey',
                'message' => 'test',
                'sendDate' => '1508144471',
                'lineNumber' => '5000222',
                'receptor' => '09111111111',
                'checkId' => '2020',
            )
        );
    }

    /**
     * @test
     */
    public function testGetData()
    {
        $data = $this->request->getData();

        $this->assertSame('apikey', $data['apiKey']);
        $this->assertSame('test', $data['message']);
        $this->assertSame('1508144471', $data['sendDate']);
        $this->assertSame('5000222', $data['lineNumber']);
        $this->assertSame('09111111111', $data['receptor']);
        $this->assertSame('2020', $data['checkId']);
    }

    /**
     * @test
     */
    public function testSend()
    {
        $this->setMockHttpResponse('MessageSuccess.txt');
        $response = $this->request->send();

        $this->assertIsBool($response->isSuccessful());
    }
}
