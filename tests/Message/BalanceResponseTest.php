<?php

/**
 * @package Payamak\Ghasedak\Tests\Message
 */

namespace Payamak\Ghasedak\Tests\Message;

use Payamak\Ghasedak\Message\BalanceRequest;
use Payamak\Ghasedak\Message\BalanceResponse;
use Payamak\Tests\TestCase;

/**
 * Class BalanceResponseTest
 */
class BalanceResponseTest extends TestCase
{
    /**
     * @test
     */
    public function testBalanceSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('BalanceSuccess.txt');
        $mockRequest = $this->createMock(BalanceRequest::class);
        $jsonContent = json_decode($httpResponse->getBody()->getContents(), true);

        $response = new BalanceResponse($mockRequest, $jsonContent);

        $this->assertIsBool($response->isSuccessful());
        $this->assertEquals("success", $response->getMessage());
    }

    /**
     * @test
     */
    public function testBalanceFailure()
    {
        $httpResponse = $this->getMockHttpResponse('BalanceFailure.txt');
        $mockRequest = $this->createMock(BalanceRequest::class);
        $jsonContent = json_decode($httpResponse->getBody()->getContents(), true);

        $response = new BalanceResponse($mockRequest, $jsonContent);

        $this->assertIsBool($response->isSuccessful());
        $this->assertSame('Invalid Parameters', $response->getMessage());
    }
}
