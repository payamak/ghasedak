<?php

namespace Payamak\Ghasedak\Tests;

use Payamak\Ghasedak\Gateway;
use Payamak\Ghasedak\Message\BalanceResponse;
use Payamak\Ghasedak\Message\MessageResponse;
use Payamak\Tests\GatewayTestCase;

class GatewayTest extends GatewayTestCase
{
    /**
     * @var Gateway
     */
    protected $gateway;

    /**
     * @var array
     */
    protected $options;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->gateway = new Gateway($this->getMethodsClient($this->getMockClient()));

        $this->options = [
            'apiKey' => 'apiKey',
            'message' => 'test',
            'sendDate' => '1508144471',
            'lineNumber' => '5000222',
            'receptor' => '09111111111',
            'checkId' => '2020',
        ];
    }

    /**
     *
     */
    public function testMessageFailure(): void
    {
        $this->setMockHttpResponse('MessageFailure.txt');

        /** @var MessageResponse $response */
        $response = $this->gateway->message($this->options)->send();

        $this->assertFalse($response->isSuccessful());
        $this->assertSame('401', $response->getCode());
        $this->assertSame('Unauthorized.Invalid Api key', $response->getMessage());
    }

    /**
     *
     */
    public function testMessageSuccess(): void
    {
        $this->setMockHttpResponse('MessageSuccess.txt');

        /** @var MessageResponse $response */
        $response = $this->gateway->message($this->options)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertSame('200', $response->getCode());
        $this->assertSame('success', $response->getMessage());
    }

    /**
     *
     */
    public function testBalanceFailure(): void
    {
        $this->setMockHttpResponse('BalanceFailure.txt');

        /** @var BalanceResponse $response */
        $response = $this->gateway->balance($this->options)->send();

        $this->assertFalse($response->isSuccessful());
        $this->assertSame('400', $response->getCode());
        $this->assertSame('Invalid Parameters', $response->getMessage());
    }

    /**
     *
     */
    public function testBalanceSuccess(): void
    {
        $this->setMockHttpResponse('BalanceSuccess.txt');

        /** @var BalanceResponse $response */
        $response = $this->gateway->balance($this->options)->send();

        $this->assertTrue($response->isSuccessful());
        $this->assertSame('200', $response->getCode());
        $this->assertSame('success', $response->getMessage());
    }
}
