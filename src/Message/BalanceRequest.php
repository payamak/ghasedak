<?php

/**
 * @package Payamak\Ghasedak\Message
 */

namespace Payamak\Ghasedak\Message;

/**
 * Class CreditRequest
 */
class BalanceRequest extends AbstractRequest
{
    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getData(): array
    {
        return [
            'apiKey' => $this->getApiKey(),
        ];
    }

    /**
     * @param mixed $data
     * @return BalanceResponse
     */
    protected function createResponse($data): BalanceResponse
    {
        return $this->response = new BalanceResponse($this, $data);
    }

    /**
     * @inheritDoc
     *
     * @param string $endpoint
     * @return string
     */
    protected function createUri(string $endpoint): string
    {
        return $endpoint . '/account/info';
    }
}
