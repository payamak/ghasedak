<?php

/**
 * @package Payamak\Ghasedak\Message
 */

namespace Payamak\Ghasedak\Message;

/**
 * Class MessageRequest
 */
class MessageRequest extends AbstractRequest
{
    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getData()
    {
        return [
            'apiKey' => $this->getApiKey(),
            'message' => $this->getMessage(),
            'sendDate' => $this->getSendDate(),
            'lineNumber' => $this->getLineNumber(),
            'receptor' => $this->getReceptor(),
            'checkId' => $this->getCheckId(),
        ];
    }

    /**
     * @param string $endpoint
     * @return string
     */
    protected function createUri(string $endpoint)
    {
        return $endpoint . '/sms/send/simpl';
    }

    /**
     * @param mixed $data
     * @return MessageResponse
     */
    protected function createResponse($data)
    {
        return $this->response = new MessageResponse($this, $data);
    }
}
