<?php

/**
 * @package Payamak\Ghasedak\Message
 */

namespace Payamak\Ghasedak\Message;

use Exception;
use Payamak\Common\Exception\InvalidResponseException as InvalidResponseExceptionAlias;
use Payamak\Common\Message\ResponseInterface;
use Payamak\Ghasedak\ParametersTrait;

/**
 * Class AbstractRequest
 */
abstract class AbstractRequest extends \Payamak\Common\Message\AbstractRequest
{
    use ParametersTrait;

    /**
     * @var string
     */
    private $endpoint = 'https://api.ghasedak.io/v2';

    /**
     * @return string
     */
    protected function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * @inheritDoc
     *
     * @param mixed $data
     * @return ResponseInterface
     * @throws InvalidResponseExceptionAlias|\Http\Client\Exception
     */
    public function sendData($data): ResponseInterface
    {
        try {
            $httpResponse = $this->httpClient->post(
                $this->createUri($this->getEndpoint()),
                [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                ],
                json_encode($data)
            );
            $json = $httpResponse->getBody()->getContents();
            $data = !empty($json) ? json_decode($json, true) : [];
            return $this->response = $this->createResponse($data);
        } catch (Exception $e) {
            throw new InvalidResponseExceptionAlias(
                'Error communicating with messaging gateway: ' . $e->getMessage(),
                $e->getCode()
            );
        }
    }

    /**
     * @param string $endpoint
     * @return string
     */
    abstract protected function createUri(string $endpoint);

    /**
     * @param mixed $data
     * @return MessageResponse
     */
    abstract protected function createResponse($data);
}
