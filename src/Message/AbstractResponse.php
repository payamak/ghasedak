<?php

/**
 * @package Payamak\Ghasedak\Message
 */

namespace Payamak\Ghasedak\Message;

/**
 * Class AbstractResponse
 */
abstract class AbstractResponse extends \Payamak\Common\Message\AbstractResponse
{
    /**
     * Response code
     *
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->data['result']['code'];
    }

    /**
     * Response Message
     *
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->data['result']['message'];
    }

    /**
     * @inheritDoc
     *
     * @return boolean
     */
    public function isSuccessful(): bool
    {
        return $this->getCode() === '200';
    }
}
