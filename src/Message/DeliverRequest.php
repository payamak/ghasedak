<?php

/**
 * @package Payamak\Ghasedak\Message
 */

namespace Payamak\Ghasedak\Message;

/**
 * Class DeliverRequest
 */
class DeliverRequest extends AbstractRequest
{
    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getData(): array
    {
        return [
            'apiKey' => $this->getApiKey(),
            'id' => $this->getId(),
            'type' => $this->getType(),
        ];
    }

    /**
     * @param mixed $data
     * @return DeliverResponse
     */
    protected function createResponse($data): DeliverResponse
    {
        return $this->response = new DeliverResponse($this, $data);
    }

    /**
     * @inheritDoc
     *
     * @param string $endpoint
     * @return string
     */
    protected function createUri(string $endpoint): string
    {
        return $endpoint . '/sms/status';
    }
}
