<?php

/**
 * @package Payamak\Ghasedak
 */

namespace Payamak\Ghasedak;

/**
 * Trait ParametersTrait
 */
trait ParametersTrait
{
    /**
     * @param string $value
     * @return $this
     */
    public function setApiKey(string $value)
    {
        $this->setParameter('apiKey', $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->getParameter('apiKey');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setMessage(string $value)
    {
        $this->setParameter('message', $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->getText() ?: $this->getParameter('message');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setSendDate(string $value)
    {
        $this->setParameter('sendDate', $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getSendDate(): string
    {
        return $this->getParameter('sendDate');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setLineNumber(string $value)
    {
        $this->setParameter('lineNumber', $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getLineNumber(): string
    {
        return $this->getFrom() ?: $this->getParameter('lineNumber');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setReceptor(string $value)
    {
        $this->setParameter('receptor', $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getReceptor(): string
    {
        return $this->getTo() ?: $this->getParameter('receptor');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setCheckId(string $value)
    {
        $this->setParameter('checkId', $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getCheckId(): string
    {
        return $this->getParameter('checkId');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setId(string $value)
    {
        $this->setParameter('id', $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->getParameter('id');
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setType(string $value)
    {
        $this->setParameter('type', $value);

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->getParameter('type');
    }
}
