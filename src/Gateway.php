<?php

/**
 * @package Payamak\Ghasedak
 */

namespace Payamak\Ghasedak;

use Payamak\Common\AbstractGateway;
use Payamak\Common\Message\RequestInterface;
use Payamak\Ghasedak\Message\BalanceRequest;
use Payamak\Ghasedak\Message\DeliverRequest;
use Payamak\Ghasedak\Message\MessageRequest;

/**
 * Class Gateway
 */
class Gateway extends AbstractGateway
{
    use ParametersTrait;

    /**
     * @inheritDoc
     * @return string
     */
    public function getName(): string
    {
        return 'Ghasedak';
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function getDefaultParameters(): array
    {
        return [
            'testMode' => false,
            'apiKey' => '',
            'lineNumber' => '',
        ];
    }

    /**
     * @inheritDoc
     * @param array $parameters
     * @return BalanceRequest
     */
    public function balance(array $parameters = []): RequestInterface
    {
        return $this->createRequest(BalanceRequest::class, $parameters);
    }

    /**
     * @inheritDoc
     * @param array $parameters
     * @return MessageRequest
     */
    public function message(array $parameters = []): RequestInterface
    {
        return $this->createRequest(MessageRequest::class, $parameters);
    }

    /**
     * @inheritDoc
     * @param array $parameters
     * @return DeliverRequest
     */
    public function deliver(array $parameters = []): RequestInterface
    {
        return $this->createRequest(DeliverRequest::class, $parameters);
    }
}
