# Payamak Ghasedak

> Ghasedak driver for the Payamak PHP library for SMS messaging.

[![Packagist Version](https://img.shields.io/packagist/v/payamak/ghasedak.svg)][1]
[![PHP from Packagist](https://img.shields.io/packagist/php-v/payamak/ghasedak.svg)][2]
[![Travis (.com) branch](https://img.shields.io/gitlab/pipeline/payamak/ghasedak/master)][3]
[![Codecov](https://img.shields.io/codecov/c/gl/payamak/ghasedak.svg)][4]
[![Packagist](https://img.shields.io/packagist/l/payamak/ghasedak.svg)][5]
[![Twitter: nekofar](https://img.shields.io/twitter/follow/nekofar.svg?style=flat)][6]

[1]: https://packagist.org/packages/payamak/ghasedak
[2]: https://www.php.net/releases/7_2_0.php
[3]: https://gitlab.com/payamak/ghasedak
[4]: https://codecov.io/gl/payamak/ghasedak
[5]: https://gitlab.com/payamak/ghasedak/blob/master/LICENSE
[6]: https://twitter.com/nekofar

[//]: https://ghasedak.io/docs